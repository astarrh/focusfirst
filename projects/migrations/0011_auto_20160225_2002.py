# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-25 20:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0010_auto_20160224_1942'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='item',
            name='awaiting_response_return_days',
        ),
        migrations.AddField(
            model_name='item',
            name='awaiting_response',
            field=models.BooleanField(default=False),
        ),
    ]
