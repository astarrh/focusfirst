from datetime import date, timedelta

from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, TemplateView, UpdateView
from django.shortcuts import render
from django.http import HttpResponseRedirect

from projects.models import Venture, Item

from .forms import AwaitingResponseForm

def awaiting_response(request):
    if request.method == 'POST':
        form = AwaitingResponseForm(request.POST)
        if form.is_valid():
            need = Item.objects.get(id=request.item_pk)
            need.awaiting_response = form.cleaned_data[0]
            need.awaiting_response_from = form.cleaned_data[1]
            need.awaiting_response_date = date.today() + timedelta(days=form.cleaned_data[2])
            return HttpResponseRedirect(reverse_lazy("awaiting_response"))
        else:
            form = AwaitingResponseForm()
        return render(request, 'awaiting_response_form.html', {'form': form})


class AllNeedsTemplateView(TemplateView):
    template_name = "all_needs.htm"

    def needs_list(self):
        return Item.objects.filter(
            type="N",
            venture__user=self.request.user,
            urgent=False,
            completed=False,
            awaiting_response=False,
        )

    def urgent_needs_list(self):
        return Item.objects.filter(
            type="N",
            venture__user=self.request.user,
            urgent=True,
            completed=False,
            awaiting_response=False,
        )

    def awaiting_response_list(self):
        return Item.objects.filter(
            type="N",
            venture__user=self.request.user,
            completed=False,
            awaiting_response=True,
        )

    def ventures_list(self):
        return Venture.objects.filter(user=self.request.user)


class AllProjectsTemplateView(TemplateView):
    template_name = "all_projects.htm"

    def projects_list(self):
        return Item.objects.filter(type="P", completed=False, venture__user=self.request.user)

    def ventures_list(self):
        return Venture.objects.filter(user=self.request.user)


class AllIdeasTemplateView(TemplateView):
    template_name = "all_ideas.htm"

    def ideas_list(self):
        return Item.objects.filter(type="I", completed=False, venture__user=self.request.user)

    def ventures_list(self):
        return Venture.objects.filter(user=self.request.user)


class VentureCreateView(CreateView):
    template_name = "venture_form.html"
    model = Venture
    fields = ["name"]

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super(VentureCreateView, self).form_valid(form)

    def ventures_list(self):
        return Venture.objects.filter(user=self.request.user)

    def get_success_url(self):
        return reverse_lazy("all_needs")


class ItemCreateView(CreateView):
    template_name = "item_form.html"
    model = Item
    fields = ['name', 'details', 'type', 'estimated_hours_to_completion']


class ItemEditView(UpdateView):
    template_name = "item_form.html"
    model = Item
    pk_url_kwarg = "item_pk"
    fields = ['name', 'details', 'type', 'estimated_hours_to_completion',
              'actual_hours_to_completion', "urgent", 'awaiting_response',
              'awaiting_response_from', "awaiting_response_date", "completed"]

    def get_success_url(self):
        return reverse_lazy("venture_view", kwargs={'pk': self.kwargs['pk']})


class NeedCreateView(CreateView):
    template_name = "item_form.html"
    model = Item
    fields = ['name', 'details', 'estimated_hours_to_completion', 'urgent', 'awaiting_response',
              'awaiting_response_date', "awaiting_response_from"]

    def form_valid(self, form):
        form.instance.type = "N"
        form.instance.venture = Venture.objects.get(id=self.kwargs['pk'])
        return super(NeedCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy("venture_view", kwargs={'pk': self.kwargs['pk']})


class ProjectCreateView(CreateView):
    template_name = "item_form.html"
    model = Item
    fields = ['name', 'details']

    def form_valid(self, form):
        form.instance.type = "P"
        form.instance.venture = Venture.objects.get(id=self.kwargs['pk'])
        return super(ProjectCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy("venture_view", kwargs={'pk': self.kwargs['pk']})


class IdeaCreateView(CreateView):
    template_name = "item_form.html"
    model = Item
    fields = ['name', 'details']

    def form_valid(self, form):
        form.instance.type = "I"
        form.instance.venture = Venture.objects.get(id=self.kwargs['pk'])
        return super(IdeaCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse_lazy("venture_view", kwargs={'pk': self.kwargs['pk']})


class VentureView(TemplateView):
    template_name = "venture.html"

    def venture(self):
        return Venture.objects.get(id=self.kwargs['pk'])

    def needs_list(self):
        return Item.objects.filter(venture=self.kwargs['pk'], type="N", completed=False)

    def projects_list(self):
        return Item.objects.filter(venture=self.kwargs['pk'], type="P", completed=False)

    def ideas_list(self):
        return Item.objects.filter(venture=self.kwargs['pk'], type="I", completed=False)

    def ventures_list(self):
        return Venture.objects.filter(user=self.request.user)

