from django import forms

from projects.models import Item

class AwaitingResponseForm(forms.Form):
    awaiting_response = forms.BooleanField(label="Awaiting Response?", initial=True)
    respondent = forms.CharField(label="Respondent", max_length=48)
    days_to_respond = forms.IntegerField(label="Days to Respond", initial=7)
