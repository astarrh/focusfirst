from datetime import date, timedelta

from django.db import models
from django.contrib.auth.models import User

class Venture(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=48)

    def __str__(self):
        return self.name

class Item(models.Model):

    TYPES = (
        ("N", "Needs"),
        ("P", "Projects"),
        ("I", "Ideas"),
    )

    name = models.CharField(max_length=48)
    venture = models.ForeignKey(Venture)
    details = models.TextField(blank=True)
    type = models.CharField(max_length=1, choices=TYPES)
    completed = models.BooleanField(default=False)
    estimated_hours_to_completion = models.FloatField(null=True, blank=True)
    actual_hours_to_completion = models.FloatField(blank=True, null=True)
    urgent = models.BooleanField(default=False)
    awaiting_response = models.BooleanField(default=False)
    awaiting_response_from = models.CharField(max_length=48, blank=True, null=True)
    awaiting_response_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.name



class Steps(models.Model):
    name = models.CharField(max_length=48)
    details = models.TextField(blank=True)
    item = models.ForeignKey(Item)
