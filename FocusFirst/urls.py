"""FocusFirst URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from django.views.generic import CreateView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required

from projects import views

urlpatterns = [

    # https://docs.djangoproject.com/en/1.8/topics/auth/default/#module-django.contrib.auth.views
    url('^accounts/', include('django.contrib.auth.urls', namespace="Users")),
    url('^register/', CreateView.as_view(
            template_name='registration/register.html',
            form_class=UserCreationForm,
            success_url='/'
    ), name="register"),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', login_required(views.AllNeedsTemplateView.as_view()), name="all_needs"),
    url(r'^projects/$', login_required(views.AllProjectsTemplateView.as_view()),
        name="all_projects"),
    url(r'^ideas/$', login_required(views.AllIdeasTemplateView.as_view()), name="all_ideas"),
    url(r'^ventures/create/$', login_required(views.VentureCreateView.as_view()),
        name="create_venture"),
    url(r'^ventures/(?P<pk>\d+)/$', login_required(views.VentureView.as_view()),
        name="venture_view"),
    url(r'^ventures/(?P<pk>\d+)/edit-item/(?P<item_pk>\d+)$', login_required(
            views.ItemEditView.as_view()), name="edit_item"),
    url(r'^ventures/(?P<pk>\d+)/add-need/$', login_required(views.NeedCreateView.as_view()),
        name="create_need"),
    url(r'^ventures/(?P<pk>\d+)/add-idea/$', login_required(views.IdeaCreateView.as_view()),
        name="create_idea"),
    url(r'^ventures/(?P<pk>\d+)/add-project/$', login_required(views.ProjectCreateView.as_view()),
        name="create_project"),
    url(r'^ventures/(?P<pk>\d+)/response/(?P<item_pk>\d+)$', login_required(
            views.awaiting_response), name="awaiting_response"),
]

