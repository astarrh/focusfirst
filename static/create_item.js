/**
 * Created by Adam on 2/9/2016.
 */

$('#modal').on('show.bs.modal', function (event) {
var modal = $(this);
$.ajax({
url: "{% url 'news-create' %}",
context: document.body
}).done(function(response) {
modal.html(response);
});
});